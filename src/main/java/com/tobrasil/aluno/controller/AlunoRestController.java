package com.tobrasil.aluno.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tobrasil.aluno.model.Aluno;
import com.tobrasil.aluno.model.RequestPostAluno;
import com.tobrasil.aluno.service.AlunoService;

@RestController
@RequestMapping("/aluno")
public class AlunoRestController {
	
	private AlunoService alunoService;
	
	public AlunoRestController(AlunoService alunoService) {
		this.alunoService = alunoService;
	}
	
	@GetMapping
	public List<Aluno> getAlunos() {
		return alunoService.buscaTodosAlunos();
	}
	
	@PostMapping
	public ResponseEntity<Aluno> saveAlunos(@Valid @RequestBody RequestPostAluno aluno) {
		return ResponseEntity.status(HttpStatus.CREATED).body(alunoService.insereAluno(aluno));
	}

}
