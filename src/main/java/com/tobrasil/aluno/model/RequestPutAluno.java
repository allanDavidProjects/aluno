package com.tobrasil.aluno.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class RequestPutAluno {
	
	@NotNull
	private Long id;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String idade;

}
