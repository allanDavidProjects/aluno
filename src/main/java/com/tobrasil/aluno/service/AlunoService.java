package com.tobrasil.aluno.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.tobrasil.aluno.model.Aluno;
import com.tobrasil.aluno.model.RequestPostAluno;
import com.tobrasil.aluno.repository.AlunoRepository;

@Service
public class AlunoService {
	
	private AlunoRepository alunoRepository;
	
	public AlunoService(AlunoRepository alunoRepository) {
		this.alunoRepository = alunoRepository;
	}
	
	public List<Aluno> buscaTodosAlunos() {
		return alunoRepository.findAll();
	}
	
	public Optional<Aluno> findById(Long id) {
		return alunoRepository.findById(id);
	}
	
	public Aluno insereAluno(RequestPostAluno postAluno) {
		Aluno aluno = Aluno.builder().nome(postAluno.getNome()).idade(postAluno.getIdade()).build();
		return alunoRepository.save(aluno);
	}

}
